import { Team } from './../models/team.model';


import { Injectable } from '@angular/core';
import { Player, Role } from '../models'; 
import { Observable} from 'rxjs';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection ,AngularFirestoreDocument } from '@angular/fire/firestore';
import { auth } from 'firebase/app';
  
@Injectable({
  providedIn: 'root'
})
export class AuthService {


   teams : Observable<Team[]>
  TeamMember :Observable<any>

  user$: Observable<any>;
  userData: any;
  private players :AngularFirestoreCollection<Player>;
  private roles :AngularFirestoreCollection<Role>;
 // private teamcollection :AngularFirestoreCollection<Team>;
 // private Playercollection :AngularFirestoreCollection<Player>;
  constructor(
    private afAuth : AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) { 
    this.players = this.afs.collection('players');
    this.roles = this.afs.collection('roles');
   this.teams = this.afs.collection('teams').valueChanges();
    this.TeamMember = this.afs.collection('players').valueChanges();
  }

  status:boolean;

  // datafetch(){
  //   let namamam= this.afs.collection().valueChanges();
  //   namamam.subscribe(console.log);
  // }
  getteam(){
      return this.teams;
    } 
    getPlayers(){
      return this.TeamMember
    }

  async googleSignin(){
    const provider = new auth.GoogleAuthProvider();
    provider.addScope('https://www.googleapis.com/auth/cloud-platform'); 
    provider.addScope('https://www.googleapis.com/auth/userinfo.profile'); 
    return this.afAuth.auth.signInWithPopup(provider).then(resp => {
      this.status=true;
      return this.updateUserData(resp.additionalUserInfo.profile)
    })
  }

  

  async signOut(){
    await this.afAuth.auth.signOut();
    this.status=false;
    localStorage.clear();
    return this.router.navigate(['login']);
  }

  private async updateUserData(player:Object){
    let playerInfo: any = await this.players.doc(player['email']).get().toPromise();
      if(playerInfo.exists) {
      playerInfo = playerInfo.data();
      playerInfo['role'] = (await playerInfo['role'].get()).data();
    //  playerInfo['team'] = (await playerInfo['team'].get()).data();
      return playerInfo;
    }
    player['role'] = this.afs.doc(`roles/QUP1JmF63UW4zmwvNbcp`).ref;
    await this.players.doc(player['email']).set(player);
    player = (await this.players.doc(player['email']).get().toPromise()).data();
    player['role'] = (await player['role'].get()).data();
 

    // player['team'] = this.afs.doc(`teams/team1`).ref;
    // await this.players.doc(player['email']).set(player);
    // player = (await this.players.doc(player['email']).get().toPromise()).data();
    // player['team'] = (await player['team'].get()).data();
    return player;
  }
}
