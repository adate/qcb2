import { DocumentReference } from '@angular/fire/firestore';

export interface Player{
    name: string,
    role: DocumentReference,
    given_name: string,
    locale:string,
    hd: string,
    family_name: string,
    picture: string
}