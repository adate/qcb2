import { Player } from './player.model';
import { Role } from './role.model';

export {
    Player,
    Role
}