

import { LoginComponent } from './routes/login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from "@angular/material";
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComponentsModule,
    MatInputModule,
    FormsModule, 
    ReactiveFormsModule,
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyCTM3OkNX-4_0hxS4nqV1Wr6Dr22FwPvsQ',
      authDomain: 'animated-radar-265712.firebaseapp.com',
      databaseURL: 'https://animated-radar-265712.firebaseio.com',
      projectId: 'animated-radar-265712',
      storageBucket: 'animated-radar-265712.appspot.com',
      messagingSenderId: '224765346328',
      appId: '1:224765346328:web:5346ca7cd6c4792136396d'
    }),
    AngularFireAuthModule,
    AngularFirestoreModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),

    // newly add
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
