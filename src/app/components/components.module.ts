import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';

const declaredComponents = [
  FooterComponent,
  HeaderComponent
]
const exports = [
  FooterComponent,
  HeaderComponent
]
const providers = [];
@NgModule({
  declarations: [
    ...declaredComponents
  ],
  imports: [
    CommonModule
  ],
  providers: [
    ...providers
  ],
  exports: [
    ...exports
  ]
})
export class ComponentsModule { }
