import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'login', loadChildren: () => import('./routes/login/login.module').then(mod => mod.LoginModule)},
  { path: 'user', loadChildren: () => import('./routes/user/user.module').then(mod => mod.UserModule)},

  { path: '**' , redirectTo: 'login'},
  { path: '*' , redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
