import { AddteamComponent } from './addteam/addteam.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { UserRoutingModule } from './user-routing.module';
import { ComponentsModule } from '../../components/components.module';
import { TdetailsComponent } from './tdetails/tdetails.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [UserComponent, DashboardComponent, ProfileComponent, TdetailsComponent, AddteamComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    ComponentsModule,
    FormsModule, 
    ReactiveFormsModule
  ]
})
export class UserModule { }
