import { AuthService } from 'src/app/service/auth.service';
import { Chart } from 'chart.js';
import { Component, OnInit } from '@angular/core';
import { timer, Observable } from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {

  constructor(private authserv : AuthService) { }

  charts = {};
  piechart = {};

  name: any;
  role: any;
  given_name: any;
  email: any;
  id: any;
  family_name: any;
  picture: any;

  //roleview:any



  bidPossible:any;  
  TotalBudget:any=2000;
  TotalSpent:any=0;
  NumofPlayer:any=0;
  




  

   MaxBid(){
    this.bidPossible=this.TotalBudget-((this.TotalSpent+(13-(this.NumofPlayer))*50));
    console.log(this.bidPossible)
    
  }


  chooseTeam(e) {
    console.log(this.parse(e.target.value))
  }
  stringify(object: Object) {
    return JSON.stringify(object);
  }
  parse(string: string) {
    return JSON.parse(string);
  }



  
  d :any;
  tname :any;
  s :any


  ngOnInit() {


  
  //   this.authserv.getteam().subscribe(tb =>{
  //     this.s=tb;
  //     console.log(this.s);
  //       tb.values()
  // })









      this.authserv.getPlayers().subscribe(teams => {
        
        this.d=teams  ;
          
        console.log(this.d);
      })
      

      this.authserv.getteam().subscribe(teamname => {
        console.log(teamname)
       this.tname=teamname  ;
          //console.log(teamname[1]);
        console.log(this.tname[0].Spend);
      })


     
      





   

    this.name = localStorage.getItem('name');
    this.email = localStorage.getItem('email');
    this.role = JSON.parse(localStorage.getItem('role'));
    this.picture = localStorage.getItem('picture');
    console.log(this.role)
    //this.roleview=this.role;



// chart

// this.chart = new Chart('canvas', {
//   type: 'line',
//   data: {
//     labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
//     datasets: [{
//       label: 'My First dataset',
//       data: [1, 2, 3, 4, 5, 6, 7, 8, 9],
//       backgroundColor: 'red',
//       borderColor: 'red',
//       fill: false,

//     }, {
//       label: 'My Second dataset',
//       backgroundColor: 'blue',
//       borderColor: 'blue',
//       fill: false,
//       data: [11, 25, 36, 42, 51, 62, 93, 71, 78, 79, 85, 95].reverse(),
//     }
//     ]
//   }
// })



this.piechart = new Chart('piechartmalefemale', {
  type: 'doughnut',
  data: {
    datasets: [{
      data: [1, 2],
      backgroundColor: [
        'blue',
        'pink',
        // 'yellow',
        // 'green',
        // 'blue',
      ],
      label: 'Dataset 1'
    }],
    labels: [
      'Male',
      'Female'
    ]
  }
})


this.piechart = new Chart('piechartmaxbid', {
  type: 'doughnut',
  data: {
    datasets: [{
      data: [10, 5],
      backgroundColor: [
        'red',
        'orange',
        // 'yellow',
        // 'green',
        // 'blue',
      ],
      label: 'Dataset 1'
    }],
    labels: [
      'Bid Done',
      'Bid Remain'
    ]
  }
})


this.piechart = new Chart('piechartbalance', {
  type: 'doughnut',
  data: {
    datasets: [{
      data: [500, 1500],
      backgroundColor: [
        'yellow',
        'purple ',
        // 'yellow',
        // 'green',
        // 'blue',
      ],
      label: 'Dataset 1'
    }],
    labels: [
      'Bid Amount',
      'Balance remain'
    ]
  }
})





  }

}
