import { AddteamComponent } from './addteam/addteam.component';


import { TdetailsComponent } from './tdetails/tdetails.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
const routes: Routes = [
  { path: '' , component: UserComponent, children: [
      { path: 'dashboard', component: DashboardComponent},
      { path: 'profile', component: ProfileComponent},
      { path: 'teamdetails', component: TdetailsComponent},
      { path: 'addteam', component: AddteamComponent},
      
      { path: '**', redirectTo: 'dashboard'}
  ]}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
