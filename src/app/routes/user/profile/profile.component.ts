import { Player } from './../../../models/player.model';
import { AuthService } from './../../../service/auth.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private auth:AuthService) { }

  
    name: any;
    role: any;
    given_name: any;
    email:any;
    id: any;
    family_name: any;
    picture: any;
    team : any;
  


  ngOnInit() {
  
   this.name = localStorage.getItem('name');
   this.email = localStorage.getItem('email');
   this.role = JSON.parse(localStorage.getItem('role'));
   this.picture = localStorage.getItem('picture');
   this.team = localStorage.getItem('team')
  }

}
