import { Player } from './../../models/player.model';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';
// import { Observable } from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  // public details: Observable<any[]>;

  constructor(private authServ: AuthService, private router:Router ) { }

  ngOnInit() {
    // if(!this.authServ.status)
    // {
    //   this.router.navigate();
    // }

  //   this.details = this.db.list("/registration").valueChanges();
  //  this.details.subscribe(console.log);




  }

  login() {
    this.authServ.googleSignin().then(
      resp => {
        for(let i in resp) {
          localStorage.setItem(i, typeof(resp[i]) == 'string' ? resp[i] : JSON.stringify(resp[i]))
        }     
        
        this.router.navigate(['/user/profile']);

        console.log(resp);
        
      }
    ).catch(err => {
      throw err;
    })
  }

}
